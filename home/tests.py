from django.test import TestCase, Client
from .models import tanyakan
from .forms import tanyaForm
# Create your tests here.


class Test(TestCase):
    def test_feedback_view(self):
        response = self.client.get('/tanya/')
        self.assertEqual(response.status_code, 200)

    def test_feedback_model(self):
        feedback = tanyakan.objects.create(
            namaPenanya="budi", emailPenanya="budi@gmail.com", pertanyaan="siapa")
        count = tanyakan.objects.all().count()
        self.assertEqual(count, 1)

    def test_feedback_template_used(self):
        response = Client().get('/tanya/')
        self.assertTemplateUsed(response, 'home/tanya.html')

    def test_send_feedback(self):
        response = Client().get('/tanya/')
        contents = response.content.decode('utf8')
        self.assertIn("Nama", contents)
        self.assertIn("Email", contents)
        self.assertIn("Pertanyaan", contents)
