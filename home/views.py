from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView
from django.urls import reverse_lazy
from .forms import tanyaForm
from .models import tanyakan
# Create your views here.


def index(request):
    return render(request, 'home/index.html')


def edukasi(request):
    return render(request, 'home/edukasi.html')


def tanya(request):
    form = tanyakan.objects.all()
    status = tanyaForm(request.POST or None)
    context = {
        'form': form,
        'status': status,
    }
    if request.method == 'POST':
        if status.is_valid():
            form.create(
                namaPenanya=status.cleaned_data.get('namaPenanya'),
                emailPenanya=status.cleaned_data.get('emailPenanya'),
                pertanyaan=status.cleaned_data.get('pertanyaan'),
            )
    return render(request, 'home/tanya.html', context)


def contacts(request):
    return render(request, 'home/contacts.html')


def admintanya(request):
    form = tanyakan.objects.all()
    status = tanyaForm(request.POST or None)
    context = {
        'form': form,
        'status': status,
    }
    if request.method == 'POST':
        if status.is_valid():
            form.create(
                namaPenanya=status.cleaned_data.get('namaPenanya'),
                emailPenanya=status.cleaned_data.get('emailPenanya'),
                pertanyaan=status.cleaned_data.get('pertanyaan'),
            )
    return render(request, 'home/admintanya.html', context)


class showPertanyaan(ListView):
    model = tanyakan
    template_name = 'home/admintanya.html'
