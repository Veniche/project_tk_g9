from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('edukasi/', views.edukasi, name='edukasi'),
    path('tanya/', views.tanya, name='tanya'),
    path('contacts', views.contacts, name='contacts'),
    path('admintanya/', views.admintanya, name='admintanya')
]
