from django.http import response, HttpResponse
from django.shortcuts import render
from .forms import feedbackForm
from .models import feedbackModel

# Create your views here.

def feedback_form(request):
    form = feedbackModel.objects.all()
    status = feedbackForm(request.POST or None)
    context = {
        'form' : form,
        'status' : status,
    }
    if request.method=='POST':
        if status.is_valid():
            form.create(
                Nama = status.cleaned_data.get('Nama'),
                Email = status.cleaned_data.get('Email'),
                Kritik = status.cleaned_data.get('Kritik'),
            )
            return render(request, 'thanks.html')
    else:
        form = feedbackForm()
    return render(request, 'feedback_form.html', context)  