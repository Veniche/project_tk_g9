from django.contrib import admin
from .models import feedbackModel
from .forms import feedbackForm

# Register your models here.

admin.site.register(feedbackModel)