from django.urls import path
from . import views
from .views import AddBeritaView, BeritaView, BeritaDetailView

app_name = 'berita'

urlpatterns = [
    path('upload', AddBeritaView.as_view(), name='upload'),
    path('', BeritaView.as_view(), name='list'),
    path('detail/<int:pk>', BeritaDetailView.as_view(), name='detail')
]