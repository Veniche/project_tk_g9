from django.shortcuts import redirect, render
from django.views.generic import ListView, DetailView, CreateView
from .models import Berita
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Create your views here.
@method_decorator(login_required(login_url='login'), name='dispatch')
class AddBeritaView(CreateView):
    model = Berita
    template_name = 'upload_berita.html'
    fields = '__all__'

class BeritaView(ListView):
    model = Berita
    template_name = 'list_berita.html'
    context_object_name = 'berita'
    paginate_by = 4
    queryset = Berita.objects.all()

class BeritaDetailView(DetailView):
    model = Berita
    template_name = 'detail_berita.html'