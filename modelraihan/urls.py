from django.urls import path
from . import views

app_name = 'modelraihan'

urlpatterns = [
    path('', views.formku, name = 'form'),
    path('register', views.register, name="register"),
    path('login', views.loginCustom, name='login'),
    path('logout', views.logoutCustom, name='logout'),

]