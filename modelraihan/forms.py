from django.forms import ModelForm
from .models import dataModel
from django import forms
from django.contrib.auth.models import User

class dataForm(forms.ModelForm) :
    class Meta:
        model = dataModel
        fields = [
            'Nama',
            'Umur',
            'AktivitasSebelumGejala',
            'Gejala',
        ]
        widgets={
            'Nama' : forms.TextInput(attrs={'class' : 'input','placeholder' : 'Bob Elpistolero'}),
            'Umur': forms.NumberInput(attrs={'step': 1, 'placeholder' : '20'}),
            'AktivitasSebelumGejala' : forms.Textarea(attrs={'class' : 'textarea', 'rows' : 5}),
            'Gejala' : forms.Textarea(attrs={'class' : 'textarea', 'rows' : 5})
        }


class RegisterForm(forms.Form):
    username = forms.CharField(
        label="Username ",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'example: Bob',
                'name': 'username',
            }
        ),
        max_length=16
    )

    email = forms.EmailField(
        label="Email ",
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'name': 'email',
            }
        )
    )

    password1 = forms.CharField(
        label="Password ",
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'name': 'password',
            }
        )
    )




class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=16,
        label="Username: ",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
            }
        )
    )

    password = forms.CharField(
        label="Password: ",
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
            }
        )
    )