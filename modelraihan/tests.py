from .apps import ModelraihanConfig
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .views import *
from .models import *
from .forms import *
import time

# Create your tests here.

class UnitTest(TestCase) :
    def setUp(self):
        dataku = dataModel.objects.create(Nama="Nama", Umur=23 , AktivitasSebelumGejala="user", Gejala="kamu-kamu")
        count = dataModel.objects.all().count()
        self.assertEqual(count,1)
    def test_url_exist(self):
        response = self.client.get('/form/')
        self.assertEqual(response.status_code,200)
    def test_funct_exist(self):
        found = resolve('/form/')
        self.assertEqual(found.func,formku)
    def test_template_used(self):
        response = Client().get('/form/')
        self.assertTemplateUsed(response, 'form.html')
    def test_model(self):
        dataku = dataModel.objects.all().count()
        self.assertEquals(dataku,1)
    def test_form_is_blank(self):
        form = dataForm(data= {'Nama':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
                form.errors['Nama'],
                ["This field is required."]
                )
    def test_form_is_valid(self) :
        formku = dataForm(data={'Nama':"bob", 'Umur':23 , 'AktivitasSebelumGejala':"user", 'Gejala':"kamu-kamu"})
        self.assertTrue(formku.is_valid())
    def test_signup_url_exists(self):
        response = Client().get('/form/register')
        self.assertEqual(response.status_code, 200)
    def test_logout_url_exist(self):
        response = Client().get('/form/logout')
        self.assertEqual(response.status_code, 302)

    def test_login_url_exist(self):
        response = Client().get('/form/login')
        self.assertEqual(response.status_code, 200)   
    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "hahaha",
            "password": "hihihihi123"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "a",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())
        
    def test_forms_register_valid(self):
        form1 = RegisterForm(data={
            "username": "",
            "email": "haha@gmail.com",
            'password1': '123321hahaha',
           
        })
        self.assertFalse(form1.is_valid())

